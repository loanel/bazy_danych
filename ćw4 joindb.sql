USE joindb
SELECT a.buyer_id AS buyer1, a.prod_id, b.buyer_id AS buyer2
FROM sales AS a
	JOIN sales AS b
	ON a.prod_id = b.prod_id
WHERE a.buyer_id <> b.buyer_id

SELECT a.buyer_id AS buyer1, a.prod_id, b.buyer_id AS buyer2
FROM sales AS a
	JOIN sales AS b
	ON a.prod_id = b.prod_id
WHERE a.buyer_id < b.buyer_id