/* 1 */
SELECT Customers.CompanyName, Categories.CategoryName, SUM([Order Details].Quantity)
FROM Customers
	JOIN Orders
	ON Orders.CustomerID = Customers.CustomerID
	JOIN [Order Details]
	ON [Order Details].OrderID = Orders.OrderID
	JOIN Products
	ON Products.ProductID = [Order Details].ProductID
	JOIN Categories
	ON Products.CategoryID = Categories.CategoryID
WHERE Categories.CategoryName IS NOT NULL
GROUP BY Customers.CompanyName, Categories.CategoryName

/*2*/

SELECT [Order Details].OrderID, Customers.CompanyName, SUM([Order Details].Quantity) 
FROM [Order Details]
	JOIN Orders
	ON Orders.OrderID = [Order Details].OrderID
	JOIN Customers
	ON Customers.CustomerID = Orders.CustomerID
GROUP BY [Order Details].OrderID, Customers.CompanyName

/* 3 */

SELECT [Order Details].OrderID, Customers.CompanyName, SUM([Order Details].Quantity) AS 'Amount'
FROM [Order Details]
	JOIN Orders
	ON Orders.OrderID = [Order Details].OrderID
	JOIN Customers
	ON Customers.CustomerID = Orders.CustomerID
GROUP BY [Order Details].OrderID, Customers.CompanyName
HAVING Sum([Order Details].Quantity)> 250

/* 4 */

SELECT Customers.CompanyName, Products.ProductName
FROM Customers
	JOIN Orders
	ON Orders.CustomerID = Customers.CustomerID
	JOIN [Order Details]
	ON [Order Details].OrderID = Orders.OrderID
	JOIN Products
	ON [Order Details].ProductID = Products.ProductID

/* 5 */

SELECT Customers.CompanyName, [Order Details].OrderID,
		CASE WHEN [Order Details].OrderID IS NULL THEN 0
							ELSE SUM([Order Details].Quantity*[Order Details].UnitPrice)
		END AS 'Cost'
FROM Customers
	LEFT JOIN Orders
	ON Orders.CustomerID = Customers.CustomerID
	LEFT JOIN [Order Details]
	ON [Order Details].OrderID = Orders.OrderID
GROUP BY Customers.CompanyName, [Order Details].OrderID

USE library
SELECT member.firstname, member.lastname, loanhist.out_date
FROM member
	LEFT JOIN loan
	ON loan.member_no = member.member_no
	LEFT JOIN loanhist
	ON loanhist.member_no = loan.member_no
WHERE loanhist.out_date IS NULL