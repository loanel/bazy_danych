SELECT Products.ProductName, Products.UnitPrice, Suppliers.CompanyName
FROM Products
	JOIN Suppliers
	ON Products.SupplierID=Suppliers.SupplierID
WHERE Products.UnitPrice > 20 AND Products.UnitPrice < 30

SELECT Products.ProductName, Products.UnitsInStock
FROM Products
	JOIN Suppliers
	ON Products.SupplierID=Suppliers.SupplierID
WHERE Suppliers.CompanyName = 'Tokyo Traders'

SELECT Customers.CompanyName, Customers.Address, Orders.OrderDate
FROM Orders
	RIGHT OUTER JOIN Customers
	ON Customers.CustomerID = Orders.CustomerID
	AND Year(Orders.OrderDate) = 1997
WHERE Orders.OrderDate IS NULL




