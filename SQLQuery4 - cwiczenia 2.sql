/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 5 orderid, productid, quantity
FROM [Order Details]
ORDER BY quantity DESC

SELECT TOP 5 WITH TIES orderid, productid, quantity
FROM [Order Details]
ORDER BY quantity DESC

SELECT COUNT (*)
FROM Employees

SELECT COUNT(reportsto)
FROM Employees


/* */

SELECT COUNT(ProductID)
FROM Products
WHERE UnitPrice > 20 OR UnitPrice < 10

SELECT MAX(UnitPrice)
FROM Products
WHERE UnitPrice < 20

SELECT MAX(UnitPrice), MIN(UnitPrice), AVG(UnitPrice)
FROM Products
WHERE QuantityPerUnit LIKE '%bottle%'

SELECT SUM(UnitPrice*Quantity)
FROM [Order Details]
WHERE OrderID = 10250

SELECT OrderID, MAX(UnitPrice) as 'MaxUnitOrdered'
FROM [Order Details]
GROUP BY OrderID
ORDER BY MaxUnitOrdered

SELECT OrderID, MAX(UnitPrice) as 'MaxUnitOrdered', MIN(UnitPrice) as 'MinUnitOrdered'
FROM [Order Details]
GROUP By OrderID

SELECT ShipVia , COUNT(ShipVia)
FROM Orders
GROUP BY ShipVia

SELECT TOP 1 ShipVia, COUNT(ShipVia) as 'Activity'
FROM Orders
WHERE YEAR(shippeddate) = 1997
GROUP BY ShipVia
ORDER by 2 DESC

SELECT OrderID
FROM [Order Details]
GROUP BY OrderID
HAVING COUNT(OrderID) > 5

SELECT CustomerID, sum(freight)
FROM Orders
WHERE YEAR(ShippedDate) = 1998 
GROUP BY CustomerID
HAVING COUNT(OrderID) > 8
ORDER BY 2 DESC






