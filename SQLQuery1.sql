/****** Script for SelectTopNRows command from SSMS  ******/
/*1*/
SELECT title, title_no
FROM title
/*2*/
SELECT title, title_no
FROM title 
WHERE title_no=10
/*3*/
SELECT member_no, fine_assessed
FROM loanhist 
WHERE fine_assessed >= 8 AND fine_assessed <= 9
/*4*/
SELECT title, title_no, author
FROM title
WHERE author IN ('Charles Dickens', 'Jane Austen')
/*5*/
SELECT title, title_no
FROM title
WHERE title LIKE '%adventures%'
/*6*/
SELECT member_no, fine_assessed, fine_paid
FROM loanhist
WHERE fine_assessed != fine_paid
/*7*/
SELECT distinct street, state
FROM adult
/* ---- CWICZENIE 2 ---- */
/*1*/
SELECT title
FROM title
ORDER BY title asc
/*2*/
SELECT member_no, isbn, fine_assessed, fine_assessed*2 as 'double fine'
FROM loanhist
WHERE fine_assessed > 0
/*3*/
SELECT firstname + ' ' + middleinitial + ' ' + lastname as 'email_name', LOWER(firstname) + LOWER(middleinitial) + LOWER(SUBSTRING(lastname, 1, 2)) as 'suggested_mail'
FROM member
WHERE lastname IN ('Anderson')
/*4*/
SELECT 'The title is:' + title + ', the title number is ' + cast(title_no as char) as 'Title'
FROM title
