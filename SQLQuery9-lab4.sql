USE Northwind
SELECT CompanyName, Phone
FROM Customers
WHERE CustomerID IN ( 
					SELECT Orders.CustomerID 
						FROM Orders
					    WHERE Year(Orders.ShippedDate) = 1997 AND (SELECT CompanyName FROM Shippers WHERE Orders.ShipVia = Shippers.ShipperID) IN ('United Package')
					)

SELECT CompanyName, Phone
FROM Customers
WHERE CustomerID IN (
					SELECT Orders.CustomerID
						FROM Orders
						WHERE Orders.OrderID IN (
												SELECT [Order Details].OrderID
												FROM [Order Details]
												WHERE [Order Details].ProductID IN (
																					SELECT Products.ProductID	
																					FROM Products
																					WHERE Products.CategoryID IN (
																												 SELECT Categories.CategoryID
																												 FROM Categories
																												 Where Categories.CategoryName IN ('Confections')
																												 )
																					)
												)
					)

SELECT CompanyName, Phone
FROM Customers
WHERE CustomerID NOT IN (
					SELECT Orders.CustomerID
						FROM Orders
						WHERE Orders.OrderID IN (
												SELECT [Order Details].OrderID
												FROM [Order Details]
												WHERE [Order Details].ProductID IN (
																					SELECT Products.ProductID	
																					FROM Products
																					WHERE Products.CategoryID IN (
																												 SELECT Categories.CategoryID
																												 FROM Categories
																												 Where Categories.CategoryName IN ('Confections')
																												 )
																					)
												)
					)

/* 2 */

SELECT DISTINCT ProductID, Quantity
FROM [Order Details] as Ord1
WHERE Ord1.Quantity = (SELECT Max(Ord2.Quantity)
						FROM [Order Details] as Ord2
						WHERE Ord1.ProductID = Ord2.ProductID
					   )
ORDER BY Ord1.ProductID DESC

SELECT ProductName, UnitPrice
FROM Products AS Pr1
WHERE UnitPrice < (SELECT AVG(UnitPrice)
				   FROM Products AS Pr2
				  )

SELECT AVG(UnitPrice)
FROM Products

SELECT ProductName, UnitPrice, CategoryID
FROM Products AS Pr1
WHERE UnitPrice < (SELECT AVG(UnitPrice)
				   FROM Products AS Pr2
				   WHERE Pr1.CategoryID = Pr2.CategoryID
				   )

SELECT AVG(UnitPrice)
FROM Products
GROUP BY CategoryID

USE Northwind
SELECT ProductName, UnitPrice, (SELECT AVG(UnitPrice) FROM Products) as AvgPrice, UnitPrice-(SELECT AVG(UnitPrice) FROM Products) as Difference
FROM Products

SELECT ProductName, 
		(SELECT CategoryName FROM Categories WHERE PR1.CategoryID = Categories.CategoryID ), UnitPrice, 
		(SELECT AVG(UnitPrice) From Products AS PR2 WHERE PR1.CategoryID = PR2.CategoryID) as AvgCatPrice, 
		UnitPrice - (SELECT AVG(UnitPrice) From Products AS PR2 WHERE PR1.CategoryID = PR2.CategoryID) as Difference
FROM Products AS PR1

SELECT OrderID, SUM(Quantity*UnitPrice) + 
FROM [Order Details]
WHERE [Order Details].OrderID = 10250
GROUP BY OrderID