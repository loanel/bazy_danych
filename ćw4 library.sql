/****** Script for SelectTopNRows command from SSMS  ******/
SELECT member.firstname, member.lastname, juvenile.birth_date, adult.state + ' ' + adult.city + ' ' + adult.street AS 'Adress'
FROM juvenile
	JOIN member
	ON juvenile.member_no = member.member_no
	JOIN adult
	ON adult.member_no = juvenile.adult_member_no

SELECT member.firstname, member.lastname, juvenile.birth_date, adult.state + ' ' + adult.city + ' ' + adult.street AS 'Adress', member2.firstname, member2.lastname
FROM juvenile
	JOIN member
	ON juvenile.member_no = member.member_no
	JOIN adult
	ON adult.member_no = juvenile.adult_member_no
	JOIN member AS member2
	ON member2.member_no = adult.member_no


