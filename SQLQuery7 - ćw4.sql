SELECT Products.ProductName, Products.UnitPrice, Categories.CategoryName, Suppliers.Address
FROM Products
	JOIN Suppliers
	ON Products.SupplierID = Suppliers.SupplierID
	JOIN Categories
	ON Products.CategoryID = Categories.CategoryID
WHERE Products.UnitPrice > 20 AND Products.UnitPrice < 30 AND Categories.CategoryName IN ('Meat/Poultry')

SELECT Products.ProductName, Products.UnitPrice, Categories.CategoryName, Suppliers.CompanyName
FROM Products
	JOIN Suppliers
	ON Products.SupplierID = Suppliers.SupplierID
	JOIN Categories
	ON Products.CategoryID = Categories.CategoryID
WHERE Categories.CategoryName IN ('Confections')

SELECT Customers.CompanyName, Customers.Phone
FROM Customers
	JOIN Orders
	ON Customers.CustomerID = Orders.CustomerID
	JOIN Shippers
	ON Orders.ShipVia = Shippers.ShipperID
WHERE Shippers.CompanyName IN ('United Package') AND Year(Orders.ShippedDate) = 1997


SELECT DISTINCT Customers.CompanyName, Customers.Phone, Categories.CategoryName
FROM Customers
	JOIN Orders
	ON Customers.CustomerID = Orders.CustomerID
	JOIN [Order Details]
	ON Orders.OrderID = [Order Details].OrderID
	JOIN Products
	ON [Order Details].ProductID = Products.ProductID
	JOIN Categories
	ON Products.CategoryID = Categories.CategoryID
WHERE Categories.CategoryName IN ('Confections')

SELECT a.FirstName + ' ' + a.LastName AS 'A', a.Title, b.FirstName + ' ' + b.LastName AS 'B', b.Title
FROM Employees AS a
	JOIN Employees AS b
	ON a.EmployeeID = b.ReportsTo
	/*
SELECT a.FirstName + ' ' + a.LastName AS 'A', a.Title
FROM Employees AS a
	JOIN Employees AS b
	ON a.EmployeeID != b.ReportsTo
	*/
SELECT a.FirstName + ' ' + a.LastName AS 'A', a.Title
FROM Employees AS a
	LEFT OUTER JOIN Employees AS b
	ON a.EmployeeID = b.ReportsTo
WHERE b.EmployeeID IS NULL






