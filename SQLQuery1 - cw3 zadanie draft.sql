SELECT Orders.OrderID, Customers.CompanyName, [Order Details].Quantity
FROM Orders
	JOIN Customers
	ON Customers.CustomerID = Orders.CustomerID
	JOIN [Order Details]
	ON [Order Details].OrderID=Orders.OrderID


SELECT ProductID, Sum(Quantity)
FROM [Order Details]
WHERE ProductID <= 3
GROUP By ProductID