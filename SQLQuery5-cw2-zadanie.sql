/* 1 */
SELECT OrderID, SUM(UnitPrice*Quantity) as 'Value'
FROM [Order Details]
GROUP BY OrderID
ORDER by Value desc

SELECT TOP 10 OrderID, SUM(UnitPrice*Quantity) as 'Value'
FROM [Order Details]
GROUP BY OrderID
ORDER by Value desc

SELECT TOP 10 WITH TIES OrderID, SUM(UnitPrice*Quantity) as 'Value'
FROM [Order Details]
GROUP BY OrderID
ORDER by Value desc

/* 2 */

SELECT ProductID, Sum(Quantity)
FROM [Order Details]
WHERE ProductID <= 3
GROUP By ProductID

SELECT ProductID, Sum(Quantity) as 'ProductAmount'
FROM [Order Details]
GROUP By ProductID

SELECT Sum(Quantity) as 'ProductAmount'
FROM [Order Details]
GROUP By ProductID

SELECT OrderID, SUM(UnitPrice*Quantity) as 'Value', Sum(Quantity)
FROM [Order Details]
GROUP BY OrderID
HAVING Sum(Quantity) > 250


/* 3 */

SELECT orderid, productid, SUM(quantity) AS total_quantity
FROM [order details]
WHERE orderid < 10250
GROUP BY orderid, productid
WITH ROLLUP
ORDER BY orderid, productid

SELECT orderid, productid, SUM(quantity) AS total_quantity
FROM [order details]
WHERE productid = 50
GROUP BY orderid, productid
WITH ROLLUP
ORDER BY orderid, productid

SELECT orderid, productid, SUM(quantity) AS total_quantity, GROUPING(orderid), GROUPING(productid)
FROM [order details]
WHERE productid < 10250
GROUP BY orderid, productid
WITH CUBE
ORDER BY orderid, productid

