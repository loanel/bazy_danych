SELECT member.firstname, member.lastname, juvenile.birth_date
FROM member, juvenile
WHERE member.member_no = juvenile.member_no

SELECT DISTINCT title.title
FROM copy
	JOIN title
	ON title.title_no = copy.title_no
WHERE copy.on_loan = 'Y'

SELECT loanhist.in_date, loanhist.out_date, loanhist.fine_paid, DATEDIFF(day, loanhist.out_date, loanhist.in_date) AS 'Length'
FROM loanhist
	JOIN title
	ON title.title_no = loanhist.title_no
WHERE title.title = 'Tao Teh King' AND loanhist.fine_paid IS NOT NULL

SELECT reservation.isbn
FROM reservation
	JOIN member
	ON member.member_no = reservation.member_no
WHERE member.firstname = 'Stephen' AND member.lastname = 'Graff' AND member.middleinitial = 'A'